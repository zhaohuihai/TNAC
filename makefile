
include makerule.mk

# all source files directories
src_root_dir := src
src_dirs := $(shell find ./$(src_root_dir) -maxdepth 10 -type d)

# .cpp source files
src_files := $(foreach dir, $(src_dirs), $(wildcard $(dir)/*.cpp) )

# all object files directories
obj_root_dir := obj
obj_files = $(patsubst %.cpp, %.o, $(src_files))
objects := $(subst $(src_root_dir),$(obj_root_dir),$(obj_files))

TARGET = bin/tn.exe

# when make is run with no arguments, create the executable
.PHONY : default vmcmain pfapack sfmt
default : $(TARGET)

# create the executable
$(TARGET) : tnmain TensorOperation
	$(CC) $(objects) $(CC_FLAGS) $(GLOBAL_LIB) -o $@;
tnmain : 
	$(MAKE) -C src
TensorOperation :
	$(MAKE) -C src/TensorOperation
	
# show
.PHONY : show
show:
	@echo "object files"
	@echo $(objects)


.PHONY : clean
clean :
	$(MAKE) -C src clean
	$(MAKE) -C src/TensorOperation clean
