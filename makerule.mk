# specify boost header file directory
#BOOST_DIR := /Users/zhaohuihai/libraries/boost_1_57_0/
#BOOST_DIR =

# setup for blas/lapack
BLAS_LAPACK_INC := -I/opt/intel/mkl/include/
BLAS_LAPACK_LIB := -L/opt/intel/mkl/lib/ -lmkl_intel_lp64 -lmkl_core -lpthread -lm -lmkl_intel_thread -lifcore 
# -liomp5
# scalapack
#scalib = $//Users/zhaohuihai/libraries/scalapack/lib/libscalapack.a
scalib = 

# set compiler
CC = icpc

# set compilation flags
CC_FLAGS := -O2 -parallel -qopenmp
#CC_FLAGS := -O2 -no-prec-div -openmp -Wno-unknown-pragmas

#REPORT = -openmp-report1
REPORT = 

OPTION = -D_mpi_use -DHAVE_MKL

# specify include directories
#GLOBAL_INC := -I./ $(BLAS_LAPACK_INC) -I$(BOOST_DIR)
GLOBAL_INC := -I./ $(BLAS_LAPACK_INC)
#
GLOBAL_LIB := $(BLAS_LAPACK_LIB) $(scalib)
