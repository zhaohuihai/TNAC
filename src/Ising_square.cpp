/*
 * Ising_square.cpp
 *
 *  Created on: 2013-9-9
 *      Author: ZhaoHuihai
 */

// C++ Standard Template Library
//	1. C library
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstdarg>
#include <cassert>
//	2. Containers
#include <vector>
//	3. Input/Output
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
//	4. Other
#include <algorithm>
#include <string>
#include <complex>
#include <utility>
//-----------------------------
// intel mkl
#include <mkl.h>

#include "parameter.h"
#include "Ising_square.h"

using namespace std ;

namespace ising {
double exact_free_energy_integrand(double beta, double x) {
  double k= 2.0*sinh(2.0*beta)/(cosh(2.0*beta)*cosh(2.0*beta));
  return log(1.0+sqrt(fabs(1.0-k*k*cos(x)*cos(x))));
}

double exact_free_energy(double temp) {
  const double pi=M_PI;
  const double beta = 1.0/temp;

  // Simpson's rule
  // integral(0.0, pi/2.0, func(x));
  double integral = 0.0;
  {
    const int n=500000;
    const double x_start = 0.0;
    const double x_end = 0.5*pi;
    const double h = (x_end-x_start)/static_cast<double>(2*n);
    double sum_even = 0.0;
    double sum_odd = 0.0;
    for(int i=0;i<n;++i) {
      sum_odd += exact_free_energy_integrand(beta, h*(2*i+1));
    }
    for(int i=1;i<n;++i) {
      sum_even += exact_free_energy_integrand(beta, h*(2*i));
    }
    integral += exact_free_energy_integrand(beta, x_start);
    integral += exact_free_energy_integrand(beta, x_end);
    integral += 2.0*sum_even + 4.0*sum_odd;
    integral *= h/3.0;
  }

  return -temp*( log(sqrt(2.0)*cosh(2.0*beta))+integral/pi );
}
} // namespace ising

IsingSquare::IsingSquare(Parameter& parameter)
{
	long int latticeLength = 2 * pow((double)3, parameter.RGsteps / 2) ;
	double nsites = (double)latticeLength * (double)latticeLength ;

	cout << "RG steps: " << parameter.RGsteps << endl ;
	cout << "lattice size: " << latticeLength << " x " << latticeLength << endl ;
	cout << "chi: " << parameter.chi << endl ;
	cout << "---------------------------------------------------" << endl ;

	double freeEnergy ;
	double energy ;
	//----------------------------------------------------------------------------------
	string dirName = "result/IsingSquare" ;
	makeDir(dirName) ;

	string fileName = "/T" + num2str(parameter.temp, 10) + "_" ;
	fileName = fileName + "L" + num2str(latticeLength) + "x" + num2str(latticeLength) + "_" ;
	fileName = fileName + "Chi" + num2str(parameter.chi) + ".txt" ;
	fileName = dirName + fileName ;

	ofstream outfile(&(fileName[0]), ios::out) ;
//	cout << fileName << endl ;
	if (!outfile) {
		cerr<<"file open error!" << std::endl ;
		exit(0) ;
	}
	outfile << setw(12) << "free energy" << setw(11) << "|" ;
	outfile << setw(12) << "energy" << setw(11) << "|" << endl ;
	//----------------------------------------------------------------------------------
	TensorArray<double> T(2) ;
	createTensorNetwork(parameter, T) ;
	EnvironmentHoneycomb env(parameter, T) ;

	freeEnergy = env.freeEnergy() / nsites ;
	freeEnergy = - freeEnergy * parameter.temp ;
	cout << "temperature: " << setprecision(15) << parameter.temp << endl ;
	cout << "free energy: " << freeEnergy << endl ;

	energy = computeEnergy(parameter, env, T) ;
	cout << "energy: " << energy << endl ;

	int wid = 21 ;
	outfile.precision(15) ;
	outfile << setw(wid) << freeEnergy << " |" ;
	outfile << setw(wid) << energy << " |" << endl ;
	int RGsteps = parameter.RGsteps ;
	outfile.close() ;
}

IsingSquare::~IsingSquare()
{

}
//==============================================================================================
//----------------------------------------------------------------------------------------------
void IsingSquare::createTensorNetwork(Parameter& parameter, TensorArray<double>& T)
{
	// H = - Si * Sj; S = 1 or -1
	double temp = parameter.temp ;
	double Beta = 1.0 / temp ;

	double expA = exp(  Beta) ;
	double expB = exp(- Beta) ;
	double sqrtSinh = sqrt((expA - expB) / 2.0) ;
	double sqrtCosh = sqrt((expA + expB) / 2.0) ;

	Tensor<double> W(2, 2) ;

	W(0, 0) =   sqrtCosh ;
	W(1, 0) =   sqrtCosh ;
	W(0, 1) =   sqrtSinh ;
	W(1, 1) = - sqrtSinh ;
	//-------------------------------------------------------
	// indices order: (up, left, down, right)
	// t(i,j,k,l) = sum{s}_[W(s,i)*W(s,j)*W(s,k)*W(s,l)]
	Tensor<double> t(2, 2, 2, 2) ;
	createSiteTensor(W, t) ;
	//-------------------------------------------------------
	square2honeycomb(t, T) ;

}

void IsingSquare::lnZ_2x2(Tensor<double>& t)
{
	Index a1, a2, a4, a5, a7, a8, a10, a11 ;

	Tensor<double> A = t ;
	A.putIndex(a1, a2, a4, a11) ;

	Tensor<double> B = t ;
	B.putIndex(a4, a5, a1, a8) ;

	Tensor<double> C = t ;
	C.putIndex(a10, a11, a7, a2) ;

	Tensor<double> D = t ;
	D.putIndex(a7, a8, a10, a5) ;

	//
	Tensor<double> M = contractTensors(A, B) ;
	M = contractTensors(M, C) ;
	M = contractTensors(M, D) ;

	cout << "log( " << M(0) << " ): " << setprecision(10) << log( M(0) ) / 4.0 << endl ;
	double lnZ = log( M(0) ) ;

}

void IsingSquare::createEnergySiteTensor(Parameter& parameter, Tensor<double>& t)
{
	// H = - Si * Sj; S = 1 or -1
	double temp = parameter.temp ;
	double Beta = 1.0 / temp ;
	double sh = sinh(Beta) ;
	double ch = cosh(Beta) ;
	double sqrtSh = sqrt(sh) ;
	double sqrtCh = sqrt(ch) ;

	Tensor<double> W(2, 2) ;
	W(0, 0) =   sqrtCh ;
	W(1, 0) =   sqrtCh ;
	W(0, 1) =   sqrtSh ;
	W(1, 1) = - sqrtSh ;
	//------------------------------------
	Tensor<double> U(2, 2) ;
	U(0, 0) = - sh / sqrtCh ;
	U(1, 0) = - sh / sqrtCh ;
	U(0, 1) = - ch / sqrtSh ;
	U(1, 1) = ch / sqrtSh ;
	//------------------------------------
	// indices order: (up, left, down, right)
	// t(i,j,k,l) = sum{s}_[W(s,i)*U(s,j)*W(s,k)*W(s,l)]
	t = Tensor<double>(2, 2, 2, 2) ;
	t = 0 ;
	for(int i = 0; i < 2 ; i ++)
	{
		for (int j = 0; j < 2 ; j ++)
		{
			for (int k = 0; k < 2; k ++)
			{
				for (int l = 0; l < 2; l ++)
				{
					for (int s = 0; s < 2; s ++)
					{
						t(i, j, k, l) += W(s,i)*U(s,j)*W(s,k)*W(s,l) ;
//						t(i, j, k, l) += W(s,j)*U(s,i)*W(s,k)*W(s,l) ;
					}
				}
			}
		}
	}
}

// t(i,j,k,l) = sum{s}_[W(s,i)*W(s,j)*W(s,k)*W(s,l)]
void IsingSquare::createSiteTensor(Tensor<double>& W, Tensor<double>& t)
{
	t = 0 ;

	for(int i = 0; i < 2 ; i ++)
	{
		for (int j = 0; j < 2 ; j ++)
		{
			for (int k = 0; k < 2; k ++)
			{
				for (int l = 0; l < 2; l ++)
				{
					for (int s = 0; s < 2; s ++)
					{
						t(i, j, k, l) += W(s,i)*W(s,j)*W(s,k)*W(s,l) ;
					}
				}
			}
		}
	}

}

// t(i,j,k,l) = sum{z}_[T0(z,i,j)*T1(z,k,l)]
void IsingSquare::square2honeycomb(Tensor<double> t, TensorArray<double>& T)
{
	// t((i,j),(k,l))
	t = t.reshape(4, 4) ;

	Tensor<double> U, S, V ;
	// t((i,j),(k,l)) = sum{z}_[U((i,j),z)*S(z)*V((k,l),z)]
	svd_qr(t, U, S, V) ;

	//***********************************
	// truncate small singular value
	truncateSmall(U, S, V) ;
	int dz = S.numel() ;
	//***********************************

	Tensor<double> sqrtS = S.sqrt() ;
	// U((i,j),z) = U((i,j),z)*sqrtS(z)
	U = absorbVector(U, 1, sqrtS) ;
	U = U.trans() ; // U(z,(i,j))
	T(0) = U.reshape(dz, 2, 2) ; // T0(z, i, j)

	// V((k,l),z) = V((k,l),z)*sqrtS(z)
	V = absorbVector(V, 1, sqrtS) ;
	V = V.trans() ; // V(z,(k,l))
	T(1) = V.reshape(dz, 2, 2) ; // T1(z, k, l)
}

void IsingSquare::truncateSmall(Tensor<double>& U, Tensor<double>& S, Tensor<double>& V)
{
	int chi = S.numel() - 1 ;

	while ( S(chi) < 1e-12 )
	{
		chi = chi - 1 ;
	}

	S = S.subTensor(0, chi) ;

	U = U.subTensor(0, U.dimension(0) - 1, 0, chi) ;
	V = V.subTensor(0, V.dimension(0) - 1, 0, chi) ;
}

double IsingSquare::computeEnergy(Parameter& parameter, EnvironmentHoneycomb& env,
		TensorArray<double>& T)
{
	//------------------------------------------------------------
	Tensor<double> tH ; // tH(i,j,k,l)
	createEnergySiteTensor(parameter, tH) ;
	TensorArray<double> Th(2) ;
	square2honeycomb(tH, Th) ;
	double energyBond ;
	Tensor<double> Menv = env.compute_Menv() ; // Menv(x0,y0,x1,y1)
	energyBond = env.computeBond(parameter, Menv, T, Th) ;

	double energySite = energyBond * 2 ;
	//------------------------------------------------------------
	return energySite ;
}
