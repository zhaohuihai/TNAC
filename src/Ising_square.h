/*
 * Ising_square.h
 *
 *  Created on: 2013年9月9日
 *      Author: ZhaoHuihai
 */

#ifndef ISING_SQUARE_H_
#define ISING_SQUARE_H_

#include <string>

#include "TensorOperation/TensorOperation.hpp"
#include "parameter.h"
#include "environment_honeycomb.h"

namespace ising {

//! Tc of the Ising model on the square lattice
const double Tc = 2.0/log(1.0 + sqrt(2.0));
double exact_free_energy(double temp);

} // namespace ising

class IsingSquare
{
private:
public:
	// constructor
	IsingSquare(Parameter& parameter) ;
	// destructor
	~IsingSquare() ;
	//-----------------------------------------------------------------------------------
	void createTensorNetwork(Parameter& parameter, TensorArray<double>& T) ;
	void lnZ_2x2(Tensor<double>& t) ;

	void createBoltzmann(Parameter& parameter, Tensor<double>& expH) ;
	void createMagWeight(Parameter& parameter, Tensor<double>& Mh) ;

	void square2honeycomb(Tensor<double> t, TensorArray<double>& T) ;
	void truncateSmall(Tensor<double>& U, Tensor<double>& S, Tensor<double>& V) ;
	void createSiteTensor(Tensor<double>& W, Tensor<double>& T) ;
	void createEnergySiteTensor(Parameter& parameter, Tensor<double>& t) ;
	//*******************************************************************
//	double computeMagnetization(Parameter& parameter, Environment& env, TensorArray<double>& T) ;
	double computeEnergy(Parameter& parameter, EnvironmentHoneycomb& env, TensorArray<double>& T) ;

	void exchangeTensors(TensorArray<double> &T) ;
	//*******************************************************************
	void createMagTensor(Parameter& parameter, TensorArray<double>& Tm) ;
	void createSpecialTensor(Parameter& parameter, Tensor<double> Ms, TensorArray<double>& Ts) ;
};


#endif /* ISING_SQUARE_H_ */
