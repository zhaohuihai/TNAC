

#include "TensorOperation/TensorOperation.hpp"
#include "Ising_square.h"
#include "parameter.h"

#include "example.h"

using namespace std ;

//=========================================================================================
/* Main function */
int main(int argc, char **argv) {

	/* Get arguments */
	if (argc < 4) std::cerr << "Usage: IsingSRG.exe chi step T\n";
	if (argc < 2) std::cerr << "Warning: Assuming chi = 10\n";
	if (argc < 3) std::cerr << "Warning: Assuming step = 10\n";
	if (argc < 4) std::cerr << "Warning: Assuming T = T_c\n";

	const int chi = ( argc < 2 ) ? 10 : atoi(argv[1]) ;
	const int step = (argc < 3 ) ? 10 : atoi(argv[2]) ;
	const double temp = (argc < 4) ? ising::Tc : atof(argv[3]) ;

	const double f_exact = ising::exact_free_energy(temp);

	Parameter parameter(chi, step, temp) ;
	IsingSquare Ising(parameter) ;

	return 0;
}
